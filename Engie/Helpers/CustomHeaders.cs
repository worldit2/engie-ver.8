﻿namespace Engie.Helpers
{
    public class CustomHeaders
    {
        private readonly RequestDelegate _next;

        public CustomHeaders(RequestDelegate next)
        {
            _next = next;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            context.Response.Headers.Add("X-Frame-Options", "sameorigin");
            context.Response.Headers.Add("X-Xss-Protection", "1; mode=block");
            context.Response.Headers.Add("X-Content-Type-Options", "nosniff");

            await _next(context);
        }
    }
}
